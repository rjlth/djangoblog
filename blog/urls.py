from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

app_name = 'blog'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'hot/$', views.HotView.as_view(), name='hot'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    url(r'^register/$', views.UserFormView.as_view(), name='register'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout,  {'next_page': '/'}, name='logout'),

    url(r'^author/(?P<pk>[0-9]+)/$', views.AuthorView.as_view(), name='author-detail'),
    url(r'^author/update$', views.AuthorUpdate.as_view(), name='author-update'),
    url(r'^author/posts/$', views.AuthorPostList.as_view(), name='author-posts'),

    url(r'post/add/$', views.PostCreate.as_view(), name='post-create'),
    url(r'post/(?P<pk>[0-9]+)/update/$', views.PostUpdate.as_view(), name='post-update'),
    url(r'post/(?P<pk>[0-9]+)/delete/$', views.PostDelete.as_view(), name='post-delete'),
    url(r'post/(?P<pk>[0-9]+)/like/$', views.PostLike.as_view(), name='post-like'),
    url(r'post/(?P<pk>[0-9]+)/comment/$', views.PostComment.as_view(), name='post-comment'),

    url(r'category/(?P<slug>[\w]+)/$', views.CategoryList.as_view(), name='category-list'),
    url(r'process-backlink', views.process_backlink, name='process-backlink'),
]
