from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.urlresolvers import reverse
# Create your models here.


class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    handle = models.CharField(max_length=200)
    rating = models.IntegerField(default=0)
    image = models.ImageField(upload_to='author/', default='author/no-img.jpg')

    @receiver(post_save, sender=User)
    def create_author(sender, instance, created, **kwargs):
        if created:
            Author.objects.create(user=instance, handle=instance.username)

    @receiver(post_save, sender=User)
    def save_author(sender, instance, **kwargs):
        instance.author.save()

    def __str__(self):
        return self.handle


class Category(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    STATUS = ((0, 'draft'), (1, 'published'))
    DRAFT = 0
    PUBLISHED = 1

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=2000)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    url = models.CharField(max_length=50, unique=True)
    image = models.ImageField(upload_to='post', default='post/no-img.jpg')
    status = models.IntegerField(choices=STATUS, default=DRAFT)
    rating = models.IntegerField(default=0)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'pk': self.pk})

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    post = models.ForeignKey('Post', on_delete=models.CASCADE)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.text


class Request(models.Model):
    text = models.TextField('Request Text')
    created_at = models.DateTimeField(auto_now_add=True)
