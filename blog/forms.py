from django.contrib.auth.models import User
from django import forms
from .models import Author, Comment


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class UserUpdateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email']

class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['handle', 'image']
