from django.shortcuts import render, redirect
from django.views import generic, View
from django.views.generic.edit import FormView
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from .forms import UserForm, AuthorForm, UserUpdateForm
from .models import Post, Comment, Category, Author, Request


# Create your views here.

class IndexView(generic.ListView):
    template_name = 'blog/index.html'
    context_object_name = 'post_list'

    def get_queryset(self):
        return Post.objects.filter(status=Post.PUBLISHED)


class HotView(generic.ListView):
    template_name = 'blog/index.html'
    context_object_name = 'post_list'

    def get_queryset(self):
        return Post.objects.filter(status=Post.PUBLISHED).order_by('-rating')


class DetailView(generic.DetailView):
    model = Post
    template_name = 'blog/detail.html'


class PostCreate(generic.CreateView):
    login_required = True
    model = Post
    fields = ['title', 'text', 'url', 'image', 'status', 'category']

    def form_valid(self, form):
        obj = form.save(commit=False)
        if self.request.user.is_authenticated():
            obj.user = self.request.user
            obj.publish()
            return super(PostCreate, self).form_valid(form)
        else:
            return redirect('blog:login')


class PostUpdate(generic.UpdateView):
    login_required = True
    model = Post
    fields = ['title', 'text', 'url', 'image', 'status', 'category']

    def form_valid(self, form):
        obj = form.save(commit=False)
        if self.request.user == obj.user:
            obj.publish()
            return super(PostUpdate, self).form_valid(form)
        else:
            return redirect('blog:login')


class PostDelete(generic.DeleteView):
    login_required = True
    model = Post
    success_url = reverse_lazy('blog:index')

    def delete(self, *args, **kwargs):
        if self.request.user is self.model.user:
            raise Exception(self.model.user)
        return super(PostDelete, self).delete(*args, **kwargs)


class PostLike(View):
    login_required = True

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            post = Post.objects.get(pk=self.kwargs.get('pk'))
            post.rating = post.rating + 1
            post.user.author.rating = post.user.author.rating + 1
            post.user.author.save()
            post.save()
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect('blog:login')


class PostComment(View):
    login_required = True

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            comment = Comment()
            comment.text = request.POST['text']
            comment.user = request.user
            comment.post = Post.objects.get(pk=self.kwargs.get('pk'))
            comment.publish()
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect('blog:login')


class UserFormView(View):
    form_class = UserForm
    template_name = 'blog/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('blog:index')

        return render(request, self.template_name, {'form': form})


class AuthorView(DetailView):
    model = User
    template_name = 'author/detail.html'


class AuthorUpdate(View):
    template_name = 'author/update_form.html'
    login_required = True

    def get(self, request):
        user_form = UserUpdateForm(instance=request.user)
        author_form = AuthorForm(instance=request.user.author)
        return render(request, self.template_name, {
            'user_form': user_form,
            'author_form': author_form
        })

    def post(self, request):
        user_form = UserUpdateForm(request.POST, instance=request.user)
        author_form = AuthorForm(request.POST, request.FILES, instance=request.user.author)

        if user_form.is_valid() and author_form.is_valid():
            user_form.save()
            author_form.save()

        return redirect('blog:author-detail', request.user.pk)


class AuthorPostList(generic.ListView):
    template_name = 'blog/post-list.html'
    context_object_name = 'post_list'

    def get_queryset(self):
        return Post.objects.filter(user=self.request.user)


class CategoryList(generic.DetailView):
    model = Category
    template_name = 'blog/index.html'
    context_object_name = 'post_list'
    slug_field = 'url'

    def get_object(self, queryset='url'):
        object = super(CategoryList, self).get_object()
        return object.post_set.all


@csrf_exempt
def process_backlink(request):
    if request.POST is not None:
        Request.objects.create(text=request.POST)
    return HttpResponse('1', content_type='text/plain')

